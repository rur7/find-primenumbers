/*
 *Program to copy a file withe all info on one line to file with 
 *atomic value on each line
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE * innfile = fopen("save.txt", "r");
	FILE * outfile = fopen("new.txt", "w");
	
	int buf;
	fscanf(innfile, "%d", &buf);
	while(!feof(innfile))
	{
		fprintf(outfile, "%d \n", buf);
		fscanf(innfile, "%d", &buf);
	}

	return 1; 
}
