/*
 * Not as easy program for finding prime numbers
 * Might be a little faster 
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "main.h"

int main(int argc, char *argv[]){
	//Takes the time of the program
	clock_t start, end;
	double cpu_time_used;
	start = clock();
	
	//Pointers to the first and last prime
	new_prime * head;	//Last, highest prime
	new_prime * tail; 	//First, lowest prime
	new_prime * scanner;//Used to get all the primes when math
	
	head = NULL;
	head = insert_prime(head, 1);
	tail = head;

	int up_to = atoi(argv[1]); //Takes input when start running program
	int i = 3; //start on 2 because we have already added 1
	int number_of_prime = 1;

	printf("Bois! Do your job!\n");
	//i devided all primes
	//if all prime is OK
	//then add
	while(i < up_to){
		scanner = tail->next; //Start from the second tail, 1 will break
		int devide = 0; //Tester to se if it can be devided
		while(scanner != NULL && devide != 1){
			devide = can_it_divide(i, scanner->number);	
			scanner = scanner->next;						
		}
		if(devide == 0){
			number_of_prime++;
			head = insert_prime(head, i);
		}
		i += 2;
		if((i % 10000) == 1){
			printf("\rOn number: %d \t Sofar  number of prime: %d",(i+1),number_of_prime);
			fflush(stdout);
		}
	}
	//print_list(tail); //Printing everything

	printf("\nNumber of primenumbers up to:%d is: %d\n",up_to, number_of_prime);
	//Ends the timer for program
	end = clock();
	cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;

	printf("Time used: %fs\n", cpu_time_used);
	printf("That was it Bois\n");

	return 0;
}

//-----FUNCTIONS-----
// 0: no, 1: yes
int can_it_divide(int the_number, int devidi){
	if(the_number % devidi == 0) return 1;
	return 0;
}

/*
 * Insert to the head of the linked list
 * Will be in desending order
 */
new_prime * insert_prime(new_prime * head, int number){
	new_prime * new_number = malloc(sizeof(new_prime));
	new_number->number = number;
	if(head == NULL)	//If there is a head
		return new_number;
	new_number->next = NULL;
	head->next = new_number;
	return new_number;
}

/*
 *Prints out all number in list
 */
void print_list(new_prime * tail){
	while(tail != NULL){
		printf("Number: %d\t", tail->number);
		tail = tail->next;
	}
}
