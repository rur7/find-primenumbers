/*
 * Easy program for finding prime numbers
 * Can be improved to be faster
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>


int main(){
	
	//Takes the time of the program
	clock_t start, end;
	double cpu_time_used;
	start = clock();

	int up_to = 1000000; 	//Change this to check up to this 
	int i = 1;			//First number
	int number_of_prime = 1;	//Counter for prime number
	printf("Number:%d\n",i);	//Easiest way to add one to prime
	while(i < up_to ){
		for(int x = 2; x < i; x++){
			if(i % x == 0) x = i;
			if(x == (i - 1)){
			   //	printf("Number: %d\n",i);
				number_of_prime++;
			}
		}
		if(i % 10000 == 0) printf("DING\n");
	i++;
	}

	printf("Total number of prime up to %d is: %d\n", i, number_of_prime);
	
	//Ends the timer for the program
	end = clock();
	cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	printf("Time used: %fs\n", cpu_time_used);
	
	printf("That was it bois\n");
	return 0;
}
