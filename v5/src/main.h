//
// Created By Runar Reve (rur7)
// Prime finder V2
//

#ifndef MAIN_H
#define MAIN_H

typedef struct prime_number_struct{
	int number;
	struct prime_number_struct * next;
}new_prime;


new_prime * insert_prime(new_prime * head, int number);

int can_it_divide(int the_number, int devidi);

void print_list(new_prime * tail);
 

#endif //MAIN_H
